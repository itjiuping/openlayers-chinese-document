"use strict";
(self.webpackChunk = self.webpackChunk || []).push([
  [1109],
  {
    20065: function (e, n, t) {
      var o = t(64254),
        s = t(7832),
        c = t(1055),
        i = t(72893);
      const r = new (t(40824).ZP)({ center: [0, 0], zoom: 2 }),
        a = (new c.Z({ layers: [new i.Z({ source: new o.Z({ key: "ApAO5ZCeRKtoMWO19d6N92rSR6_9ieItpQsvp0eB7Cw8v5N_q0QrV6e4_nQpt5ay", imagerySet: "RoadOnDemand" }) })], target: "map", view: r }), new s.Z({ projection: r.getProjection(), tracking: !0 }));
      a.once("change:position", function () {
        r.setCenter(a.getPosition()), r.setResolution(2.388657133911758);
      });
    },
  },
  function (e) {
    var n;
    (n = 20065), e((e.s = n));
  },
]);
//# sourceMappingURL=mobile-full-screen.js.map
